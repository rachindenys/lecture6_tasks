﻿using AutoMapper;
using FakeItEasy;
using Lecture5_BLL.Exceptions;
using Lecture5_BLL.Services;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Project;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.Team;
using Lecture5_Common.DTO.User;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Lecture5_BLL_Tests
{
    public class LinqServiceTests : IDisposable
    {
        readonly AcademyDbContext context;
        readonly DbContextOptions<AcademyDbContext> options;
        readonly LinqService linqService;
        readonly IMapper mapper;

        public LinqServiceTests()
        {
            options = new DbContextOptionsBuilder<AcademyDbContext>()
                            .UseInMemoryDatabase("LinqServiceTestDB")
                            .Options;
            context = new AcademyDbContext(options);
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Project, ProjectDTO>().ReverseMap();
                cfg.CreateMap<Lecture5_DAL.Entities.Task, TaskDTO>().ReverseMap();
                cfg.CreateMap<User, UserDTO>().ReverseMap();
                cfg.CreateMap<Team, TeamDTO>().ReverseMap();
                cfg.CreateMap<TaskState, TaskStateDTO>().ReverseMap();
            });
            mapper = config.CreateMapper();
            linqService = new LinqService(context, mapper);
        }

        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async void CountTasksOfCurrentUser_WhenUserHaveTwoProjectsWithTasks_ThenReturnDictionaryWithTwoKeyValuePair()
        {
                var projects = new List<Project>
                {
                    new Project { Id=1, AuthorId = 1},
                    new Project { Id=2, AuthorId = 1}
                };
                var users = new List<User>
                {
                    new User { Id = 1 }
                };

                var tasks = new List<Task>
                {
                    new Task { Id = 1, ProjectId = 1, PerformerId = 1 },
                    new Task { Id = 2, ProjectId = 2, PerformerId = 1}
                };

                context.AddRange(projects);
                context.AddRange(users);
                context.AddRange(tasks);
                context.SaveChanges();
                var result = await linqService.CountTasksOfCurrentUser(1);
                Assert.Equal(2, result.Count);
        }

        [Fact]
        public async void CountTasksOfCurrentUser_WhenUserHaveNoProjects_ThenThrowNotFoundException()
        {
                var projects = new List<Project>
                {
                    new Project { Id=1, AuthorId = 2},
                    new Project { Id=2, AuthorId = 3}
                };
                var users = new List<User>
                {
                    new User { Id = 1 }
                };

                var tasks = new List<Task>
                {
                    new Task { Id = 1, ProjectId = 1, PerformerId = 1 },
                    new Task { Id = 2, ProjectId = 2, PerformerId = 1}
                };

                context.AddRange(projects);
                context.AddRange(users);
                context.AddRange(tasks);
                context.SaveChanges();
                await Assert.ThrowsAsync<NotFoundException>(() => linqService.CountTasksOfCurrentUser(1));
        }


        [Fact]
        public async void GetTasksOfCurrentUser_WhenThreeTasksWithNameLengthLess45_ThenReturnAllTasks()
        {
            var projects = new List<Project>
                {
                    new Project { Id=1, AuthorId = 1},
                    new Project { Id=2, AuthorId = 2}
                };
            var users = new List<User>
                {
                    new User { Id = 1 }
                };

            var tasks = new List<Task>
                {
                    new Task { Id = 1, ProjectId = 1, PerformerId = 1, Name = "Test str"},
                    new Task { Id = 2, ProjectId = 2, PerformerId = 1, Name = "Test str"},
                    new Task { Id = 3, ProjectId = 1, PerformerId = 1, Name = "Test str" },
                    new Task { Id = 4, ProjectId = 2, PerformerId = 2, Name = "Test str" },
                    new Task { Id = 5, ProjectId = 1, PerformerId = 3, Name = "Test str" },
                };

            context.AddRange(projects);
            context.AddRange(users);
            context.AddRange(tasks);
            context.SaveChanges();
            var result = await linqService.GetTasksOfCurrentUser(1);
            Assert.Equal(3, result.Count);
        }

        [Fact]
        public async void GetTasksOfCurrentUser_WhenTwoTasksWithNameLengthLess45And2TasksWithNameLengthOverOrEqual45_ThenReturn2Tasks()
        {
            var projects = new List<Project>
                {
                    new Project { Id=1, AuthorId = 1},
                    new Project { Id=2, AuthorId = 2}
                };
            var users = new List<User>
                {
                    new User { Id = 1 }
                };

            var tasks = new List<Task>
                {
                    new Task { Id = 1, ProjectId = 1, PerformerId = 1, Name = "Good"},
                    new Task { Id = 2, ProjectId = 2, PerformerId = 1, Name = "Good"},
                    new Task { Id = 3, ProjectId = 1, PerformerId = 1, Name = "BadBadBadBadBadBadBadBadBadBadBadBadBadBadBad" },
                    new Task { Id = 4, ProjectId = 2, PerformerId = 1, Name = "BadBadBadBadBadBadBadBadBadBadBadBadBadBadBadBad" },
                    new Task { Id = 5, ProjectId = 1, PerformerId = 3, Name = "Test str" },
                };

            context.AddRange(projects);
            context.AddRange(users);
            context.AddRange(tasks);
            context.SaveChanges();
            var result = await linqService.GetTasksOfCurrentUser(1);
            foreach(var item in result)
            {
                Assert.True(item.Name.Length < 45);
            }
            Assert.Equal(2, result.Count);
        }

        [Fact]
        public async void GetFinishedThisYearTasksOfCurrentUser_WhenTwoTasksFinishedThisYear_ThenReturn2Tasks()
        {
            var projects = new List<Project>
                {
                    new Project { Id=1, AuthorId = 1},
                    new Project { Id=2, AuthorId = 2},
                    new Project { Id=3, AuthorId = 1}
                };
            var users = new List<User>
                {
                    new User { Id = 1 },
                    new User { Id = 2 },
                    new User { Id = 3 }
                };

            var tasks = new List<Task>
                {
                    new Task { Id = 1, ProjectId = 1, PerformerId = 1, FinishedAt = new DateTime(2020, 12, 31), Name = "Bad"},
                    new Task { Id = 2, ProjectId = 2, PerformerId = 1, FinishedAt = null, Name = "Bad"},
                    new Task { Id = 3, ProjectId = 1, PerformerId = 1, FinishedAt = new DateTime(2021, 8, 30), Name = "Good"},
                    new Task { Id = 4, ProjectId = 2, PerformerId = 1, FinishedAt = new DateTime(2021, 1, 1), Name = "Good"},
                    new Task { Id = 5, ProjectId = 1, PerformerId = 3, FinishedAt = new DateTime(2021, 2, 12), Name = "Bad"},
                };

            context.AddRange(projects);
            context.AddRange(users);
            context.AddRange(tasks);
            context.SaveChanges();
            var result = await linqService.GetFinishedThisYearTasksOfCurrentUser(1);
            foreach(var item in result)
            {
                Assert.True(item.Item2 == "Good");
            }
            Assert.Equal(2, result.Count);
        }

        [Fact]
        public async void GetFinishedThisYearTasksOfCurrentUser_WhenUserNotHaveFinishedThisYearTasks_ThenThrowNotFoundException()
        {
            var projects = new List<Project>
                {
                    new Project { Id=1, AuthorId = 1},
                    new Project { Id=2, AuthorId = 2},
                    new Project { Id=3, AuthorId = 1}
                };
            var users = new List<User>
                {
                    new User { Id = 1 },
                    new User { Id = 2 },
                    new User { Id = 3 }
                };

            var tasks = new List<Task>
                {
                    new Task { Id = 1, ProjectId = 1, PerformerId = 1, FinishedAt = new DateTime(2020, 12, 31), Name = "Bad"},
                    new Task { Id = 2, ProjectId = 2, PerformerId = 1, FinishedAt = null, Name = "Bad"},
                    new Task { Id = 3, ProjectId = 1, PerformerId = 1, FinishedAt = new DateTime(2019, 8, 30), Name = "Bad"},
                    new Task { Id = 4, ProjectId = 2, PerformerId = 1, FinishedAt = new DateTime(2022, 1, 1), Name = "Bad"},
                    new Task { Id = 5, ProjectId = 1, PerformerId = 3, FinishedAt = new DateTime(2021, 2, 12), Name = "Bad"},
                };

            context.AddRange(projects);
            context.AddRange(users);
            context.AddRange(tasks);
            context.SaveChanges();
            await Assert.ThrowsAsync<NotFoundException>(() => linqService.GetFinishedThisYearTasksOfCurrentUser(1));
        }

        [Fact]
        public async void GetTeamsWithUsersOlderThanTen_WhenTwoTeamsWithUsersOlderTen_ThenReturnTwoTeamsWithThreeSortedUsers()
        {
            var users = new List<User>
                {
                    new User { Id = 1, BirthDay = new DateTime(2011, 1, 1), RegisteredAt = new DateTime(2016, 1, 2), TeamId = 1}, //bad
                    new User { Id = 2, BirthDay = new DateTime(2010, 1, 1), RegisteredAt = new DateTime(2015, 1, 2), TeamId = 1}, //good
                    new User { Id = 3, BirthDay = new DateTime(2009, 1, 1), RegisteredAt = new DateTime(2014, 1, 2), TeamId = 1}, //good
                    new User { Id = 4, BirthDay = new DateTime(2008, 1, 1), RegisteredAt = new DateTime(2013, 1, 2), TeamId = 2}, //good
                    new User { Id = 5, BirthDay = new DateTime(2016, 2, 2), RegisteredAt = new DateTime(2017, 4, 4), TeamId = 3 } //bad
                };

            var teams = new List<Team>
            {
                new Team { Id = 1 },
                new Team { Id = 2 },
                new Team { Id = 3 }
            };
            context.AddRange(teams);
            context.AddRange(users);
            context.SaveChanges();
            var result = await linqService.GetTeamsWithUsersOlderThanTen();
            var usersCount = 0;
            var teamsCount = 0;
            foreach (var item in result)
            {
                foreach (var user in item.UsersOlder10)
                {
                    Assert.True(user.BirthDay.Year + 10 < DateTime.Now.Year);
                    usersCount++;
                }
                teamsCount++;
            }
            Assert.Equal(3, usersCount);
            Assert.Equal(2, teamsCount);
        }

        [Fact]
        public async void GetTeamsWithUsersOlderThanTen_WhenNoUsers_ThenThrowNotFoundException()
        {
            var users = new List<User>
                {
                    new User { Id = 1, BirthDay = new DateTime(2012, 1, 1), RegisteredAt = new DateTime(2016, 1, 2), TeamId = 1}, //bad
                    new User { Id = 2, BirthDay = new DateTime(2012, 1, 1), RegisteredAt = new DateTime(2015, 1, 2), TeamId = 1}, //bad
                    new User { Id = 3, BirthDay = new DateTime(2013, 1, 1), RegisteredAt = new DateTime(2014, 1, 2), TeamId = 1}, //bad
                    new User { Id = 4, BirthDay = new DateTime(2014, 1, 1), RegisteredAt = new DateTime(2013, 1, 2), TeamId = 2}, //bad
                    new User { Id = 5, BirthDay = new DateTime(2015, 2, 2), RegisteredAt = new DateTime(2017, 4, 4), TeamId = 3 } //bad
                };

            var teams = new List<Team>
            {
                new Team { Id = 1 },
                new Team { Id = 2 },
                new Team { Id = 3 }
            };
            context.AddRange(teams);
            context.AddRange(users);
            context.SaveChanges();
            await Assert.ThrowsAsync<NotFoundException>(() => linqService.GetTeamsWithUsersOlderThanTen());
        }

        [Fact]
        public async void GetSortedUserByAscendingAndTasksByDescending_WhenTwoUsersEachWithToTask_ThenReturnSortedUsersAndTasks()
        {
            var users = new List<User>
                {
                    new User { Id = 1, FirstName = "Z"}, 
                    new User { Id = 2, FirstName = "A"},
                    new User { Id = 3, FirstName = "V" }
                };
            var tasks = new List<Task>
            {
                new Task { Id = 1, PerformerId = 1, Name = "123"},
                new Task { Id = 2, PerformerId = 1, Name = "12345" },
                new Task { Id = 3, PerformerId = 2, Name = "09876" },
                new Task { Id = 4, PerformerId = 2, Name = "098" }
            };
            context.AddRange(tasks);
            context.AddRange(users);
            context.SaveChanges();
            var result = await linqService.GetSortedUserByAscendingAndTasksByDescending();
            var resultAsList = result.ToList();
            Assert.Equal(2, resultAsList.Count);
            Assert.True(resultAsList[0].UserName == "A" && resultAsList[1].UserName == "Z");
            foreach(var user in resultAsList)
            {
                var nameLen = user.SortedTasks[0].Name.Length;
                foreach(var task in user.SortedTasks)
                {
                    Assert.True(task.Name.Length <= nameLen);
                    nameLen = task.Name.Length;
                }
            }
        }

        [Fact]
        public async void GetSortedUserByAscendingAndTasksByDescending_WhenNoTasks_ThenReturnNotFoundException()
        {
            var users = new List<User>
                {
                    new User { Id = 1, FirstName = "Z"},
                    new User { Id = 2, FirstName = "A"},
                    new User { Id = 3, FirstName = "V" }
                };
            context.AddRange(users);
            context.SaveChanges();
            await Assert.ThrowsAsync<NotFoundException>(() => linqService.GetSortedUserByAscendingAndTasksByDescending());
        }

        [Fact]
        public async void AnalyzeUserProjectsAndTasks_WhenCorrectData_ThenCheckExpectedWithActual()
        {
            var projects = new List<Project>
            {
                new Project { Id = 1, AuthorId = 1, CreatedAt = new DateTime(2016, 1, 1) },
                new Project { Id = 2, AuthorId = 1, CreatedAt = new DateTime(2017, 1, 1) }, // last project
                new Project { Id = 3, AuthorId = 2, CreatedAt = new DateTime(2018, 1, 1) }
            };

            var users = new List<User>
                {
                    new User { Id = 1},
                    new User { Id = 2}
                };
            var tasks = new List<Task>
            {
                new Task { Id = 1, PerformerId = 1, FinishedAt = null, ProjectId = 2},
                new Task { Id = 2, PerformerId = 1, FinishedAt = null, ProjectId = 2},
                new Task { Id = 3, PerformerId = 2, ProjectId = 3},
                new Task { Id = 4, PerformerId = 1, CreatedAt = new DateTime(2016,1,1), FinishedAt = new DateTime(2017,1,1), ProjectId = 2},
                new Task { Id = 5, PerformerId = 2, ProjectId = 1},
                new Task { Id = 6, PerformerId = 1, CreatedAt = new DateTime(2015,1,1), FinishedAt = new DateTime(2017,1,1), ProjectId = 2} // Longest
            };
            var expectedResult = new UserAnalyzeDTO
            {
                User = new UserDTO { Id = 1 },
                LastProject = new ProjectDTO { Id = 2, AuthorId = 1, CreatedAt = new DateTime(2017, 1, 1) },
                TotalTasksCount = 4,
                TotalUncompletedAndCanceledTasks = 2,
                LongestTask = new TaskDTO { Id = 6, PerformerId = 1, CreatedAt = new DateTime(2015, 1, 1), FinishedAt = new DateTime(2017, 1, 1), ProjectId = 3 }
            };
            context.AddRange(projects);
            context.AddRange(tasks);
            context.AddRange(users);
            context.SaveChanges();
            var result = await linqService.AnalyzeUserProjectsAndTasks(1);
            Assert.True(result.User.Id == expectedResult.User.Id
                        && result.LastProject.Id == expectedResult.LastProject.Id
                        && result.TotalTasksCount == expectedResult.TotalTasksCount
                        && result.TotalUncompletedAndCanceledTasks == expectedResult.TotalUncompletedAndCanceledTasks
                        && result.LongestTask.Id == expectedResult.LongestTask.Id);
        }

        [Fact]
        public async void AnalyzeUserProjectsAndTasks_WhenUserNotFound_ThenThrowNotFoundException()
        {

            var users = new List<User>
                {
                    new User { Id = 1},
                    new User { Id = 2 }
                };
            context.AddRange(users);
            context.SaveChanges();
            await Assert.ThrowsAsync<NotFoundException>(() => linqService.AnalyzeUserProjectsAndTasks(3));
        }

        [Fact]
        public async void AnalyzeProjectTasksAndTeam_WhenCorrectData_ThenCheckExpectedWithActual()
        {
            var projects = new List<Project>
            {
                new Project { Id = 1, AuthorId = 1, TeamId = 1, Description = "123456789112345678901"},
            };

            var users = new List<User>
                {
                    new User { Id = 1, TeamId = 1},
                    new User { Id = 2, TeamId = 1}
                };
            var tasks = new List<Task>
            {
                new Task { Id = 1, PerformerId = 1, Description = "123",       ProjectId = 1, Name = "1111"},
                new Task { Id = 2, PerformerId = 1, Description = "12345",     ProjectId = 1, Name = "1111"},
                new Task { Id = 3, PerformerId = 2, Description = "123456",    ProjectId = 1, Name = "11"},
                new Task { Id = 4, PerformerId = 1, Description = "1234567",   ProjectId = 1, Name = "1111"},
                new Task { Id = 5, PerformerId = 2, Description = "12345678",  ProjectId = 1, Name = null},
                new Task { Id = 6, PerformerId = 1, Description = "123456789", ProjectId = 1, Name = null} 
            };

            var teams = new List<Team>
            {
                new Team { Id = 1}
            };
            var expectedResult = new ProjectAnalyzeDTO
            {  
                Project = new ProjectDTO { Id = 1, AuthorId = 1, TeamId = 1, Description = "123456789112345678901" },
                TotalTeamCount = 2,
                LongestTaskByDescription = new TaskDTO { Id = 6, PerformerId = 1, Description = "123456789", ProjectId = 1 },
                ShortestTaskByName = new TaskDTO { Id = 3, PerformerId = 2, Description = "123456", ProjectId = 1, Name = "11" }
            };
            context.AddRange(projects);
            context.AddRange(tasks);
            context.AddRange(users);
            context.AddRange(teams);
            context.SaveChanges();
            var result = await linqService.AnalyzeProjectTasksAndTeam(1);
            Assert.True(result.Project.Id == expectedResult.Project.Id
                        && result.TotalTeamCount == expectedResult.TotalTeamCount
                        && result.LongestTaskByDescription.Id == expectedResult.LongestTaskByDescription.Id
                        && result.ShortestTaskByName.Id == expectedResult.ShortestTaskByName.Id);
        }

        [Fact]
        public async void AnalyzeProjectTasksAndTeam_WhenProjectNotFound_ThenThrowNotFoundException()
        {
            var projects = new List<Project>
                {
                    new Project { Id = 1},
                    new Project { Id = 2}
                };
            context.AddRange(projects);
            context.SaveChanges();
            await Assert.ThrowsAsync<NotFoundException>(() => linqService.AnalyzeUserProjectsAndTasks(3));
        }

        [Fact]
        public async void GetUncompletedTasks_WhenCorrectData_ThenReturnAllUncompletedTasksForUser()
        {
            var projects = new List<Project>
                {
                    new Project { Id = 1, AuthorId = 1},
                    new Project { Id = 2, AuthorId = 1},
                    new Project { Id = 3, AuthorId = 2}
                };
            var users = new List<User>
            {
                new User { Id = 1 },
                new User { Id = 2 }
            };
            var tasks = new List<Task>
            {
                new Task { Id = 1, ProjectId = 1, PerformerId = 1, State = TaskState.Cancelled},
                new Task { Id = 2, ProjectId = 2, PerformerId = 1, State = TaskState.Finished},
                new Task { Id = 3, ProjectId = 3, PerformerId = 2, State = TaskState.Cancelled},
                new Task { Id = 4, ProjectId = 1, PerformerId = 2, State = TaskState.Created},
                new Task { Id = 5, ProjectId = 2, PerformerId = 1, State = TaskState.Started},
                new Task { Id = 6, ProjectId = 3, PerformerId = 1, State = TaskState.Cancelled},
                new Task { Id = 7, ProjectId = 1, PerformerId = 2, State = TaskState.Finished},
                new Task { Id = 8, ProjectId = 2, PerformerId = 1, State = TaskState.Created},
                new Task { Id = 9, ProjectId = 3, PerformerId = 2, State = TaskState.Started},
                new Task { Id = 10, ProjectId = 1, PerformerId = 1, State = TaskState.Cancelled},
            };
            context.AddRange(users);
            context.AddRange(projects);
            context.AddRange(tasks);
            context.SaveChanges();
            var result = await linqService.GetUncompletedTasks(1);
            var expectedIds = new List<int> { 1, 5, 6, 8, 10};
            foreach (var item in result)
            {
                Assert.Contains(item.Id, expectedIds);
                Assert.True(item.State != TaskStateDTO.Finished);
            }
        }

        [Fact]
        public async void GetUncompletedTasks_WhenUserDoesNotExists_ThenThrowNotFoundException()
        {
            var users = new List<User>
            {
                new User { Id = 1 },
                new User { Id = 2 }
            };
            context.AddRange(users);
            context.SaveChanges();
            await Assert.ThrowsAsync<NotFoundException>(() => linqService.GetUncompletedTasks(3));
        }
    }
}
