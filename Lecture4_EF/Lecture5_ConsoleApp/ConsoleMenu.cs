﻿using Lecture5_ConsoleApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Lecture5_ConsoleApp
{
    public static class ConsoleMenu
    {
        private static LinqHttpService _linqService = new LinqHttpService();
        private static TasksHttpService _tasksService = new TasksHttpService();

        private static List<Option> options = new List<Option>
            {
                new Option("1. Count tasks of current user", async () => await CountTasksOfCurrentUser()),
                new Option("2. Get all tasks of user with name length < 45", async () => await GetTasksOfCurrentUser()),
                new Option("3. Get finished this year tasks of user", async () => await GetFinishedThisYearTasksOfCurrentUser()),
                new Option("4. Get teams with sorted by registration date users, older than 10", async () => await GetTeamsWithUsersOlderThanTen()),
                new Option("5. Sort by user first name and task name", async () => await GetSortedUserByAscendingAndTasksByDescending()),
                new Option("6. Analyze user's last project and tasks", async () => await AnalyzeUserProjectsAndTasks()),
                new Option("7. Analyze project's tasks and team", async () => await AnalyzeProjectTasksAndTeam()),
                new Option("8. Mark random task as completed", async () => await MarkRandomTaskWithDelay())
            };

        public static async Task<string> CountTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var result = await _linqService.CountTasksOfCurrentUser(userID);
            var outputString = "\n\n\nResult: ";
            foreach (var item in result)
            {
                outputString += $"Projectid: {item.Key}, Tasks: {item.Value}\n";
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }

        public static async Task<string> GetTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var result = await _linqService.GetTasksOfCurrentUser(userID);
            var outputString = "";
            Console.WriteLine();
            foreach (var item in result)
            {
                outputString += $"\n\nName: {item.Name}\nDescription: {item.Description}\n";
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }

        public static async Task<string> GetFinishedThisYearTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);

            var result = await _linqService.GetFinishedThisYearTasksOfCurrentUser(userID);
            var outputString = "";
            foreach (var item in result)
            {
                outputString += $"\n\nTask ID: {item.Item1}\nTask Name: {item.Item2}\n";
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }

        public static async Task<string> GetTeamsWithUsersOlderThanTen()
        {
            Console.Clear();
            var result = await _linqService.GetTeamsWithUsersOlderThanTen();
            var outputString = "";
            foreach (var item in result)
            {
                outputString += $"\n\nTeam ID: [{item.Id}]. Team name: {item.TeamName}. Users: \n\n";
                foreach (var user in item.UsersOlder10)
                {
                    outputString += $"\t\tUser name: {user.FirstName} User Birth Day: {user.BirthDay} User Registration Date: {user.RegisteredAt}\n";
                }
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }

        public static async Task<string> GetSortedUserByAscendingAndTasksByDescending()
        {
            Console.Clear();
            var result = await _linqService.GetSortedUserByAscendingAndTasksByDescending();
            Console.WriteLine();
            var outputString = "";
            foreach (var item in result)
            {
                outputString += $"\n\nUser First Name: {item.UserName}. Tasks: \n\n";
                foreach (var task in item.SortedTasks)
                {
                    outputString += $"{task.Name}\n\n";
                }
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }

        public static async Task<string> AnalyzeUserProjectsAndTasks()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var result = await _linqService.AnalyzeUserProjectsAndTasks(userID);
            var outputString = "";
            Console.WriteLine();
            outputString += $"\n\nUser: {result.User.FirstName} {result.User.LastName}\n";
            outputString += $"Last Project: {result.LastProject?.Name}\n";
            outputString += $"Total tasks count: {result.TotalTasksCount}\n";
            outputString += $"Uncompleted and canceled tasks count: {result.TotalUncompletedAndCanceledTasks}\n";
            outputString += $"Longest task: {result.LongestTask?.Name}\n";
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }

        public static async Task<string> AnalyzeProjectTasksAndTeam()
        {
            Console.Clear();
            Console.WriteLine("Write project ID: ");
            Int32.TryParse(Console.ReadLine(), out int projectID);
            var outputString = "";
            var result = await _linqService.AnalyzeProjectTasksAndTeam(projectID);
            Console.WriteLine();
            outputString += $"\n\nProject: {result.Project?.Name}\n";
            outputString += $"Longest task by description: { result.LongestTaskByDescription?.Description}\n";
            outputString += $"Shortest task by name: {result.ShortestTaskByName?.Name}\n";
            outputString += $"Total team count where project description length > 20 or tasks count < 3: {result.TotalTeamCount}\n";
            return outputString;
        }
        public static async Task<string> MarkRandomTaskWithDelay(int delay = 1000)
        {
            var taskId = await RandomTaskMarker.MarkRandomTaskWithDelay(delay);
            return $"\n\n\nRandom task with ID {taskId} marked as finished!";
        }



        public static async Task Start()
        {
            int index = 0;
            var outputString = "";
            ConsoleKeyInfo keyinfo;
            try
            {
                do
                {
                    if(outputString != "")
                    {
                        Console.Clear();
                        Console.WriteLine(outputString);
                        Console.ReadKey();
                        outputString = "";
                    }
                    WriteMenu(options, options[index]);
                    keyinfo = Console.ReadKey();

                    // Handle each key input (down arrow will write the menu again with a different selected item)
                    if (keyinfo.Key == ConsoleKey.DownArrow)
                    {
                        if (index + 1 < options.Count)
                        {
                            index++;
                            WriteMenu(options, options[index]);
                        }
                    }
                    if (keyinfo.Key == ConsoleKey.UpArrow)
                    {
                        if (index - 1 >= 0)
                        {
                            index--;
                            WriteMenu(options, options[index]);
                        }
                    }
                    // Handle different action for the option
                    if (keyinfo.Key == ConsoleKey.Enter)
                    {

                        var selected = options[index].Selected;
                        selected.Invoke().ContinueWith(async (result) => outputString += await result);
                        index = 0;
                    }
                }
                while (keyinfo.Key != ConsoleKey.X);
            }
            catch (Exception e)
            {
                Console.WriteLine($"You can't perfom this operation: {e.Message}");
                Console.ReadKey();
                return;
            }
            Console.ReadKey();
        }

        static void WriteMenu(List<Option> options, Option selectedOption)
        {
            Console.Clear();

            foreach (Option option in options)
            {
                if (option == selectedOption)
                {
                    Console.Write("> ");
                }
                else
                {
                    Console.Write(" ");
                }

                Console.WriteLine(option.Name);
            }
        }
        protected class Option
        {
            public string Name { get; }
            public Func<Task<string>> Selected { get; }

            public Option(string name, Func<Task<string>> selected)
            {
                Name = name;
                Selected = selected;
            }
        }
    }

}
